package co.uk.development.schumacher;

/**
 largest prime factor
 */
public class Problem3 {
    public static void main(String[] args){

        long current = 2;
        long queryNumber = 600851475143L;
        long result = 0;
        while(queryNumber > 1){
            while((queryNumber % current) == 0){
               if(result < current)
                   result = current;
                queryNumber /= current;
            }
            current += 1;
        }


        System.out.print(result);

    }
}
