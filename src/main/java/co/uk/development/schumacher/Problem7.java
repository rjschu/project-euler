package co.uk.development.schumacher;

/**
 * 10_001st prime
 */
public class Problem7 {

    public static void main(String[] args){
       int p = 0;
       int n = 10_000;
       int counter = 1;
       boolean done = false;
       while(p <= n){
           counter = counter + 1;
           if(isPrime(counter)){
              p = p+1;
           }
       }
        System.out.println(counter);
    }

    public static boolean isPrime(int num){
        if(num == 2){
            return true;
        }
        else if((num % 2) == 0){
            return false;
        }

        int sqrt = (int) Math.sqrt(num);
        int i = 3;
        while(i <= sqrt){
            if((num % i) == 0){
                return false;
            }
            i++;
        }
        return true;
    }
}
