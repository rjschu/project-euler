package co.uk.development.schumacher;

/**
 * User: Roy
 * Date: 20/08/13
 * Time: 20:49
 * Comment: sum square difference
 */
public class Problem6 {

    public static int sumOfSquaresFirstHundred = 0;
    public static int sumOfSquaresSecondHundred = 0;

    public static void main(String[] args){
        int p;
        for(int i = 1; i <= 100; i++){
            sumOfSquaresFirstHundred += i*i;
        }

        for(int j = 1; j <= 100; j++){
            sumOfSquaresSecondHundred += j;
        }
        int total = sumOfSquaresSecondHundred * sumOfSquaresSecondHundred;

        System.out.print(total - sumOfSquaresFirstHundred);
    }

}
