package co.uk.development.schumacher;

/**
 * even fibanaci numbers
 */
public class Problem2 {

    public static void main(String[] args){
        int old = 1;
        int temp = 1;
        int fib = 0;
        int i =0;
       while(i < 4000000){
            if((temp % 2) == 0){
               fib += temp;
            }
            temp = old + i;
            old = i;
            i = temp;
        }
        System.out.print(fib);
    }
}
