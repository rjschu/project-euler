package co.uk.development.schumacher;

/** smallest multiple
 */
public class Problem5 {

    public static void main(String[] rgs){
        boolean d = false;
        int result = 1;
        while(!d){
            for(int i = 1; i <= 20; i++){
                if((result % i) == 0){
                    if(i == 20){
                        d = true;
                    }
                }else{
                    break;
                }
            }
            result++;
        }
        System.out.print(result-1);
    }
}
